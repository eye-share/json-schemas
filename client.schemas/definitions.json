{
    "$schema": "http://json-schema.org/draft-04/schema",
    "title": "Common definitions used accross JSON files",
    "type": "object",
    "definitions": {
		"catalogDescription": {
			"title": "Catalog of fields that can be used, with the fields config"
		},
		"modelmapDescription": {
			"title": "Where on the model to get the data, for example 'Head.SerialNumber'",
			"type": "string"
		},
		"useDefinition": {
			"type": "object",
			"title": "Define which fields to show, and in what order they should appear",
			"examples": ["'Save: 10'", "{'HiddenItem': false}", "{'Undo': 20}"],
			"patternProperties": {
				"^[A-Za-z_][A-Za-z0-9_]*$": {
					"type": ["number", "boolean"],
					"title": "Use 'false' to hide, or set a numeric value to set the column index"
				}
			}
		},
		"fieldDefinition": {
			"type": "object",
			"title": "Configure how a field should be presented",
			"properties": {
				"modelMap": {
					"$ref": "#/definitions/modelmapDescription"
				},
				"defaults": {
					"title": "Default setting blocks to use",
					"description": "Setting block read from defaults.sys.json, to avoid having to add a lot of repetitive config",
					"examples": ["f.Head:f.CodeWide:f.Required", "f.UtcDate", "f.LookupDialog"],
					"type": "string"
				},
				"validators": {
					"type": "object",
					"description": "Client side validators to use for this field"
				},
				"keepOnReimport": {
					"title": "Value to keep for reimport of document",
					"type": ["string", "boolean"],
					"examples": ["Head.Department.Key"]
				},
				"buttons": {"type": "object", "description": "Adds buttons to the field"},
				"disabled": {"type": "string", "description": "Expression - if evaluated to true, the menu item is disabled", "examples": ["!hasCreate || isNew || hasChanges()"]},
				"label": {"type": "string", "description": "Language file code. Only needed if not the same as the current config field"},
				"readonly": {"type": "boolean"},
				"type": { "type":"string", "description": "Field types", "enum": ["render", "typeahead", "int", "date", "boolean", "money", "icon", "datetime"]},
				"config": {
					"type": "object",
					"description": "Additional configuration of the field",
					"properties": {
						"accounting": {
							"type": "object",
							"properties": {
								"precision": {
									"type": "integer",
									"description": "Number of digigts after decimal used in presentation and rounding"
								}
							}
						},
						"context": {},
						"empty": {},
						"expression": {},
						"filter": {},
						"formatter": {},
						"freeText": { "type": "boolean"},
						"inputExpression": {},
						"list": { "description": "Array of result items (if set together with lookup function, these will be joined)" },
						"lookup": {},
						"lookupButton": { "type": "boolean"},
						"max": {},
						"optionExpression": {},
						"parser": {},
						"sorter": {},
						"sorterArgs": {},
						"typeMap": {},
						"uniqueKey": {}
					},
					"oneOf": [
						{
							"if": {
								"properties": { "type": { "const": "typeahead" } }
							},
							"then": {
								"properties": {
									"optionExpression":{"type": "string", "description": "Interpolation expression to use for rendering the ng-model to the input"},
									"inputExpression": {}
								}
							}
						},{
							"if": {
								"properties": { "type": { "const": "date" } }
							},
							"then": {
								"properties": {
									"isUtc":{}
								}
							}
					}]
				}
			}
		},
		"gridDefinition": {
			"type": "object",
			"properties": {
				"columns": {
					"title": "Columns in the grid / lines",
					"properties": {
						"catalog": {
							"$ref": "#/definitions/catalogDescription",
							"patternProperties": {
								"^[A-Za-z_][A-Za-z0-9_]*$": {
									"$ref": "#/definitions/fieldDefinition"
								}
							}
						},
						"use": {
							"$ref": "#/definitions/useDefinition"
						}
					}
				},
				"menu": {
					"title": "Menu buttons for the grid",
					"properties": {
						"catalog": {
							"$ref": "#/definitions/catalogDescription",
							"patternProperties": {
								"^[A-Za-z_][A-Za-z0-9_]*$": {
									"properties": {
										"action": {
											"type": "string",
											"description": "Which javascript method to call when the button is pressed"
										},
										"class": {
											"type": "string"
										},
										"direction": {
											"type": "string"
										},
										"disabled": {
											"type": ["string", "boolean"],
											"description": "If- and when the button is not clickable"
										},
										"dropDownMenu": {
											"properties": {
												"controller": {
													"type": "string"
												},
												"title": {
													"type": "string"
												},
												"items": {
													"patternProperties": {
														"^[A-Za-z_][A-Za-z0-9_]*$": {
															"$ref": "#/definitions/gridDropDownMenuItem"
														}
													}
												}
											}
										},
										"fiexedDirection": {
											"type": "boolean"
										},
										"hidden": {
											"type": "boolean",
											"description": "If- and when the button is not visible"
										},
										"hotkey": {
											"type": "string"
										},
										"iconClass": {
											"type": "string",
											"description": "Which image to use for the button"
										},
										"keepCellFocus": {
											"type": "boolean",
											"description": ""
										},
										"register": {
											"type": ["boolean"]
										},
										"type": {
											"type": "string"
										}
									}
								}
							}
						}
					}
				}
			}
		},
		"gridDropDownMenuItem": {
			"properties": {
				"checked": {
					"type": "string"
				},
				"label": {
					"type": "string"
				},
				"action": {
					"type": "string",
					"description": "Which javascript function to be run once selected"
				}
			}
		}
	}
}