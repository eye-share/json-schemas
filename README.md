# JSON Schemas for config files.

In order to use one of the schemas in a config file, paste "$schema": "url" into the root of the JSON.

The url needs to point to one of the entry files, which further points to schema files.

## For back-end, use
```
"$schema": "https://bitbucket.org/eye-share/json-schemas/raw/master/config.sys.schemas/config.sys.schema.entry.json",
```

## For client / front-end, use:
```
"$schema": "https://bitbucket.org/eye-share/json-schemas/raw/master/client.schemas/client.entry.json",
```

## Develop locally:

1. Clone down repo, ideally to c/development_next/projects
2. Point the $schema variable to the local entry file  
NB! Only relative filepath seems to work  
e.g. if this repo is in c/development_next/projects and customer repo is in c/development_next/projects/eye-share-scenario then relative filepath becomes:  
```../../../../../json-schemas/config.sys.schemas/config.sys.schema.entry.json```
3. Point the entry file to the local file you're working on.
e.g. ```"$ref": "config.sys.schema.datasets.json"```

Open configs in VS code when developing locally.
It seems like visual studio does not properly read local schemas.